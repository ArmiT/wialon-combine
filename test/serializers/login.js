/*jslint node, bitwise */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

// const chai = require("chai");
// const {describe, it} = require("mocha");

// const {expect} = chai;

// const {makeLoginRequest} = require("../src/serializers/login");

// const identifier = "";
// const passwd = "";

// exempli gracia -----------------------------------

// // login
// const loginReq = makeLoginRequest(seq)
//     .setId(id, type)
//     .setPwd(pwd. type)
//     .setProtocol(1)
//     .build();

// // keep alive

// cosnt kareq = makeKeepAliveRequest(seq)
//     .build();

// // data

// // alternate
const cpSubrecords = makeCustomParameters([
    {num, type, value, denominatorOrder}, 
    {num, type, value, denominatorOrder}
])
    .addParameter({num, type, value, denominatorOrder})
    .addUint8(num, value, denominatorOrder)
    .addUint16(num, value, denominatorOrder)
    .addUnit32(num, value, denominatorOrder)
    .addUint64(num, value, denominatorOrder)
    .addInt8(num, value, denominatorOrder)
    .addInt16(num, value, denominatorOrder)
    .addInt32(num, value, denominatorOrder)
    .addInt64(num, value, denominatorOrder)
    .addFloat(num, value)
    .addDouble(num, value)
    .addString(num, value);

// const position = makePosition({
//     lat,
//     lon,
//     speed,
//     course,
//     height,
//     sats,
//     hdop
// })
//     .geo(lat, lon)
//     .lat(-90.000000)
//     .lon(-180.000000)
//     .speed()
//     .course()
//     .height()
//     .sats()
//     .hdop();


cosnt dreq = makeDataRequest(seq)
    .addCustom(time, [cpSubrecords])
    .addPosition(time, [position])
        .addIO(time, [io])
        .addPicture(time, [picture])
        .addLBS(time, [lbs])
        .addFuel(time, [fuel])
        .addTemperature(time, [temperature])
        .addCAN(time, [can])
        .addCounter(time, [counter])
        .addAnalog(time, [analog])
        .addDriverCode(time, [driverCode])
        .addTachoFile(time, [tachoFile])
        .addDriverMessage(time, [driverMessage])
    .build();

//------------------------------------------------------------------

// describe("serializers/login", function () {
//     it(
//         "must correctly serialize login request with default parameters", 
//         function () {
//             return expect(makeLoginRequest().to.eql([
                
//             ]));
//         }
//     );
// });
