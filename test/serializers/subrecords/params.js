/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {paramType, makeParameters, calcRecordSize} = require(
    "../../../src/serializers/subrecords/params"
);

describe("serializers/subrecords/params", function () {
    it(
        "must correctly calculate size in bytes for a proposed int", 
        function () {
            const param = {
                num: 1,
                type: paramType.uint32,
                value: 4294967294
            };
            const expectedSize = 6;

            return expect(calcRecordSize(param)).to.eql(expectedSize);
        }
    );

    it(
        "must correctly calculate size in bytes for a string", 
        function () {
            const param = {
                num: 1,
                type: paramType.string,
                value: "test RTU v.1.0.0"
            };
            const expectedSize = 19;

            return expect(calcRecordSize(param)).to.eql(expectedSize);
        }
    );

    it(
        "must correctly calculate size in bytes for a extended num", 
        function () {
            const param = {
                num: 129,
                type: paramType.uint8,
                value: 0
            };
            const expectedSize = 4;

            return expect(calcRecordSize(param)).to.eql(expectedSize);
        }
    );

    it(
        "must correctly apply proposed values",
        function () {
            const result = makeParameters().addParameter({
                num: 1,
                type: paramType.uint8,
                value: 123,
                denominatorOrder: 0
            }).build();

            const expected = Buffer.from([
                0x01,
                0x00,
                0x7B
            ]);

            console.log(result);

            return expect(result).to.eql(expected);
        }
    );
});
