/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const makePosition = require("../../../src/serializers/subrecords/position");

describe("serializers/subrecords/position", function () {
    it(
        "must correctly apply proposed values in fluent mode",
        function () {
            const result = Buffer.from([

                // lat - 56000000
                0x03,
                0x56,
                0x7E,
                0x00,

                // lon - 55000000
                0x03,
                0x47,
                0x3B,
                0xC0,

                // speed - 60 км/ч
                0x00,
                0x3C,

                // course - 45 deg
                0x00,
                0x2D,

                // height - 261
                0x01,
                0x05,

                // satellites number - 9
                0x09,

                // hdop - 90 (0.9)
                0x00,
                0x5A
            ]);

            const subrecord = makePosition({hdop: 90})
                .lat(56.000000)
                .lon(55.000000)
                .speed(60)
                .course(45)
                .height(261)
                .sats(9);

            return expect(subrecord.build()).to.eql(result);
        }
    );

    it(
        "failed validation should trigger error",
        function () {
            const subrecord = makePosition()
                .lat(56.000000)
                .lon(55.000000)
                .speed(-60)
                .course(45)
                .height(261)
                .sats(9)
                .hdop(90);

            return expect(subrecord.build).to.throw();
        }
    );
});
