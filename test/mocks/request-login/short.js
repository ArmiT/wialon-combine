/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package short
// protocol version 1
// ID length 2 bytes
// PWD length 2 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    //start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // sequence lsb
    0x01,

    // data length msb
    0x00,

    // data length lsb
    0x06,

    // protocol version 1
    0x01,

    // flags id=2 pwd=2 0b00010001
    0x11,

    // ID msb
    0x00,

    // ID lsb
    0x01,

    // PWD msb
    0x1E,

    // PWD lsb
    0x61,

    // CRC msb
    0x5E,

    // CRC lsb
    0x0E
]);
