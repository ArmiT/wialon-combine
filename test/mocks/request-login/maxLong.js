/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package long
// protocol version 1
// ID 8 bytes
// PWD 8 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    // start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // seq lsb
    0x01,

    // length msb
    0x00,

    // length lsb
    0x12,

    // protocol version
    0x01,

    // flags id=8 pwd=8 0b00110011
    0x33,

    // id - 18446744073709551615
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,

    // pwd - 18446744073709551615
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,

    // crc msb
    0x28,

    // crc lsb
    0x4D
]);
