/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package without password
// protocol version 1
// ID 2 bytes
// PWD 0 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    // start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // seq lsb
    0x01,

    // length msb
    0x00,

    // length lsb
    0x04,

    // protocol version
    0x01,

    // flags id=2 pwd=0 0b00010000
    0x10,

    // id - 1
    0x00,

    // id - 2
    0x01,

    // crc msb
    0xB6,

    // crc lsb
    0xDE
]);
