/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package long
// protocol version 1
// ID 8 bytes
// PWD 8 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    // start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // seq lsb
    0x01,

    // length msb
    0x00,

    // length lsb
    0x12,

    // protocol version
    0x01,

    // flags id=8 pwd=8 0b00110011
    0x33,

    // id1
    0x01,

    // id2
    0x02,

    // id3
    0x03,

    // id4
    0x04,

    // id5
    0x05,

    // id6
    0x06,

    // id7
    0x07,

    // id lsb
    0x08,

    // pwd1
    0x08,

    // pwd2
    0x07,

    // pwd3
    0x06,

    // pwd4
    0x05,

    // pwd5
    0x04,

    // pwd6
    0x03,

    // pwd7
    0x02,

    // pwd lsb
    0x01,

    // crc msb
    0x40,

    // crc lsb
    0x0D
]);
