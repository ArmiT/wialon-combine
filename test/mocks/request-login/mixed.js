/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package mixed
// protocol version 1
// ID 4 bytes
// PWD 4 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    // start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // seq lsb
    0x01,

    // length msb
    0x00,

    // length lsb
    0x0B,

    // protocol version
    0x01,

    // flags id=4, pwd=string 0 terminated, 0b00100100
    0x24,

    // id - 1
    0x01,

    // id - 2
    0x02,

    // id - 3
    0x03,

    // id lsb
    0x04,

    // pwd - t
    0x74,

    // pwd - e
    0x65,

    // pwd - s
    0x73,

    // pwd - t
    0x74,

    // pwd terminate
    0x00,

    // crc msb
    0x78,

    // crc lsb
    0x76
]);
