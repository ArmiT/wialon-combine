/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login package int
// protocol version 1
// ID 4 bytes
// PWD 4 bytes

module.exports = Buffer.from([

    // start msb
    0x24,

    // start lsb
    0x24,

    // type - login
    0x00,

    // sequence msb
    0x00,

    // seq lsb
    0x01,

    // length msb
    0x00,

    // length lsb
    0x0A,

    // protocol version
    0x01,

    // flags id=4 pwd=4 0b00100010
    0x22,

    // id1
    0x01,

    // id2
    0xFF,

    // id3
    0xFF,

    // id lsb
    0xFF,

    // pwd1
    0x01,

    // pwd2
    0x02,

    // pwd3
    0x03,

    // pwd lsb
    0x04,

    // crc msb
    0xFA,

    // crc lsb
    0xDD
]);
