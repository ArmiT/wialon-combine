/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

module.exports = Buffer.from([

    // incorrect start-bytes
    0x24,
    0x21,

    // type - keep-alive
    0x02,

    // sequence
    0x00,
    0x01
]);
