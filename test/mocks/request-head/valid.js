/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// valid head
module.exports = Buffer.from([

    // start bytes
    0x24,
    0x24,

    // type - keep-alive (1 byte),
    0x02,

    // sequence
    0x00,
    0x01
]);
