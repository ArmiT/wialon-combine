/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// valid head
module.exports = Buffer.from([

    // start bytes
    0x24,
    0x24,

    // type
    0x01,

    // sequence
    0x00,
    0x01,

    // double length (4 byte)
    0x80,
    0x00,
    0x00,
    0x01,

    // payload
    0x01,

    // crc
    0x2D,
    0xEA
]);
