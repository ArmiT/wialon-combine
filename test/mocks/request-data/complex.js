/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// data package
module.exports = Buffer.from([

    // HEADER
    // start condition
    0x24,
    0x24,

    // type - data
    0x01,

    // sequence
    0x00,
    0x01,

    // length 89
    0x00,
    0x59,

    // DATA PACKAGE 1
    // time 1462531045 (06.05.16 10:37:25 GMT)
    0x57,
    0x2C,
    0x73,
    0xE5,

    // sub-records count - 2
    0x02,

    // sub-record type - position data
    0x01,

    // POSITION DATA SUB-RECORD
    // lat - 56000000
    0x03,
    0x56,
    0x7E,
    0x00,

    // lon - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed - 60 км/ч
    0x00,
    0x3C,

    // course - 45 deg
    0x00,
    0x2D,

    // height - 261
    0x01,
    0x05,

    // satellites number - 9
    0x09,

    // hdop - 90 (0.9)
    0x00,
    0x5A,

    // second sub-record type - custom parameters
    0x00,

    // CUSTOM PARAMS SUB-RECORD
    // count - 2
    0x02,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00011 (8 bytes)
    0x03,

    //value - 54510525546669
    0x00,
    0x00,
    0x31,
    0x93,
    0xB8,
    0x83,
    0x00,
    0xAD,

    // custom param 2
    // №2
    0x02,

    // type sensor 001 (10^1) 00010 (4 bytes)
    0x22,

    //value - 411254 (normalized 41125.4)
    0x00,
    0x06,
    0x46,
    0x76,

    // DATA PACKAGE
    // time 1520766811 (11 March 2018 г., 11:13:31)
    0x5A,
    0xA5,
    0x0F,
    0x5B,

    // sub-records count - 2
    0x02,

    // 1 sub-record type - position data
    0x01,

    // POSITION DATA SUB-RECORD
    // lat - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // lon - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed - 48 км/ч
    0x00,
    0x30,

    // course - 255 deg
    0x00,
    0xFF,

    // height - 32
    0x00,
    0x20,

    // satellites number - 14
    0x0E,

    // hdop - 110 (1.1)
    0x00,
    0x6E,

    // second sub-record type - custom parameters
    0x00,

    // CUSTOM PARAMS SUB-RECORD
    // count - 3
    0x03,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00110 (4 bytes)
    0x06,

    //value (-1500000)
    0xFF,
    0xE9,
    0x1C,
    0xA0,

    // custom param 2
    // №2
    0x02,

    // type sensor 110 (10^6) 00111 (8 bytes)
    0xC7,

    //value (9223372036854775807) (normalized 9223372036854.775807)
    0x7F,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,

    // custom param 3
    // №3
    0x03,

    // type sensor 000 (10^0) 01010 (string 0 terminated)
    0x0A,

    //value (test)
    0x74,
    0x65,
    0x73,
    0x74,
    0x00,

    // crc
    0x7D,
    0x67
]);
