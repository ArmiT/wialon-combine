/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// data package - two position sub-records
module.exports = Buffer.from([

    // start
    0x24,
    0x24,

    // type - data
    0x01,

    // sequence
    0x00,
    0x01,

    // length
    0x00,
    0x29,

    // DATA PACKAGE
    // time 1520766811 (11 March 2018 г., 11:13:31)
    0x5A,
    0xA5,
    0x0F,
    0x5B,

    // sub-records count - 2
    0x02,

    // 1 sub-record type - position data
    0x01,

    // POSITION DATA SUB-RECORD
    // lat - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // lon - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed - 48 км/ч
    0x00,
    0x30,

    // course - 255 deg
    0x00,
    0xFF,

    // height - 32
    0x00,
    0x20,

    // satellites number - 14
    0x0E,

    // hdop - 110 (1.1)
    0x00,
    0x6E,

    // 2 sub-record type - position data
    0x01,

    // POSITION DATA SUB-RECORD
    // lat - 56000000
    0x03,
    0x56,
    0x7E,
    0x00,

    // lon - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed - 60 км/ч
    0x00,
    0x3C,

    // course - 45 deg
    0x00,
    0x2D,

    // height - 261
    0x01,
    0x05,

    // satellites number - 9
    0x09,

    // hdop - 90 (0.9)
    0x00,
    0x5A,

    // crc
    0x28,
    0xA5
]);
