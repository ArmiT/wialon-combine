/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// data package - one message with two sub-records
module.exports = Buffer.from([

    // HEADER
    // start condition
    0x24,
    0x24,

    // type - data
    0x01,

    // sequence
    0x00,
    0x01,

    // length 119
    0x00,
    0x77,

    // DATA PACKAGE
    // time - 1462531045 (06.05.16 10:37:25 GMT)
    0x57,
    0x2C,
    0x73,
    0xE5,

    // sub-records count - 2
    0x02,

    // 1 sub-record type - position data
    0x01,

    // POSITION DATA SUB-RECORD
    // lat 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // lon 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed 48 км/ч
    0x00,
    0x30,

    // course 255 deg
    0x00,
    0xFF,

    // height 32
    0x00,
    0x20,

    // satellites number 14
    0x0E,

    // hdop 110 (1.1)
    0x00,
    0x6E,

    // 2 sub-record type - custom parameters
    0x00,

    // CUSTOM PARAMS SUB-RECORD
    // count - 15
    0x0F,

    // param 1
    // type sensor 0000 (10^1) 0001 (2 bytes)
    // value 5454
    0x01,
    0x01,
    0x15,
    0x4E,

    // param 2
    // type sensor 0000 (10^1) 0011 (8 bytes)
    // value 49646411459
    0x02,
    0x03,
    0x00,
    0x00,
    0x00,
    0x0B,
    0x8F,
    0x28,
    0x1E,
    0xC3,

    // param 3
    // type sensor 0000 (10^1) 0011 (8 bytes)
    // value - 54510525546669
    0x03,
    0x03,
    0x00,
    0x00,
    0x31,
    0x93,
    0xB8,
    0x83,
    0x00,
    0xAD,

    // param 4
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 411254
    0x04,
    0x02,
    0x00,
    0x06,
    0x46,
    0x76,

    // param 5
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 758689
    0x05,
    0x02,
    0x00,
    0x0B,
    0x93,
    0xA1,

    // param 6
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 223369
    0x06,
    0x02,
    0x00,
    0x03,
    0x68,
    0x89,

    // param 7
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value  - 542699
    0x07,
    0x02,
    0x00,
    0x08,
    0x47,
    0xEB,

    // param 8
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 12258899
    0x08,
    0x02,
    0x00,
    0xBB,
    0x0E,
    0x53,

    // param 9
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 98982222
    0x09,
    0x02,
    0x05,
    0xE6,
    0x59,
    0x4E,

    // param 10
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 8852699
    0x0A,
    0x02,
    0x00,
    0x87,
    0x14,
    0xDB,

    // param 11
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 454444
    0x0B,
    0x02,
    0x00,
    0x06,
    0xEF,
    0x2C,

    // param 12
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 125548
    0x0C,
    0x02,
    0x00,
    0x01,
    0xEA,
    0x6C,

    // param 13
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 145548
    0x0D,
    0x02,
    0x00,
    0x02,
    0x38,
    0x8C,

    // param 14
    // type sensor 0000 (10^1) 0010 (4 bytes)
    // value - 585241
    0x0E,
    0x02,
    0x00,
    0x08,
    0xEE,
    0x19,

    // param 15
    // type sensor 0000 (10^1) 0001 (2 bytes)
    // value - 21411
    0x0F,
    0x01,
    0x53,
    0xA3,

    // crc
    0xA3,
    0xE1
]);
