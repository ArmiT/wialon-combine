/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
// ID 2 bytes
// PWD 0 bytes

module.exports = Buffer.from([

    // protocol version
    0x01,

    // flags id=2 pwd=0 0b00010000
    0x10,

    // id - 1
    0x00,

    // id - 2
    0x01
]);
