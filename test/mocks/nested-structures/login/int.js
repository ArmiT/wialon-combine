/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
// ID 4 bytes
// PWD 4 bytes

module.exports = Buffer.from([

    // protocol version
    0x01,

    // flags id=4 pwd=4 0b00100010
    0x22,

    // id1
    0x01,

    // id2
    0xFF,

    // id3
    0xFF,

    // id lsb
    0xFF,

    // pwd1
    0x01,

    // pwd2
    0x02,

    // pwd3
    0x03,

    // pwd lsb
    0x04
]);
