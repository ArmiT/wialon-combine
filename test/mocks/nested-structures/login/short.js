/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
// ID 2 bytes
// PWD 2 bytes

module.exports = Buffer.from([

    // protocol version 1
    0x01,

    // flags id=2 pwd=2 0b00010001
    0x11,

    // ID msb
    0x00,

    // ID lsb
    0x01,

    // PWD msb
    0x1E,

    // PWD lsb
    0x61
]);
