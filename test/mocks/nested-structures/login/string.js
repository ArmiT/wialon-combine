/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
// ID 3 bytes - 123
// PWD 4 bytes - test

module.exports = Buffer.from([

    // protocol version
    0x01,

    // flags id=string 0 terminated, pwd= string 0 terminated, 0b01000100
    0x44,

    // id - 1
    0x31,

    // id - 2
    0x32,

    // id - 3
    0x33,

    // id terminate
    0x00,

    // pwd - t
    0x74,

    // pwd - e
    0x65,

    // pwd - s
    0x73,

    // pwd - t
    0x74,

    // pwd terminate
    0x00
]);
