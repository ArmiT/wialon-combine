/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
module.exports = Buffer.from([

    // protocol version
    0x01,

    // flags id=8, pwd=string 0 terminated
    0x34,

    // ID = 354705082125945
    0x00,
    0x01,
    0x42,
    0x9a,
    0x36,
    0x6b,
    0x8e,
    0x79,

    // pwd - t
    0x74,

    // pwd - e
    0x65,

    // pwd - s
    0x73,

    // pwd - t
    0x74,

    // pwd terminate
    0x00
]);
