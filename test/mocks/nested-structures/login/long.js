/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// login nested structure
// protocol version 1
// ID 8 bytes
// PWD 8 bytes

module.exports = Buffer.from([

    // protocol version
    0x01,

    // flags id=8 pwd=8 0b00110011
    0x33,

    // ID = 354705082125945
    0x00,
    0x01,
    0x42,
    0x9a,
    0x36,
    0x6b,
    0x8e,
    0x79,

    // PWD = 354705082125945
    0x00,
    0x01,
    0x42,
    0x9a,
    0x36,
    0x6b,
    0x8e,
    0x79
]);
