/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - signed int

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00110 (4 bytes)
    0x06,

    //value (-1500000)
    0xFF,
    0xE9,
    0x1C,
    0xA0
]);
