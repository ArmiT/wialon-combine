/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - string 0 terminated

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 01010 (string 0 terminated)
    0x0A,

    //value (test)
    0x74,
    0x65,
    0x73,
    0x74,
    0x00
]);
