/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - signed long

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00111 (8 bytes)
    0x07,

    //value (-3000000000)
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0x4D,
    0x2F,
    0xA2,
    0x00
]);
