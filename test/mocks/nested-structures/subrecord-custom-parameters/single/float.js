/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - float

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 01000 (4 bytes)
    0x08,

    //value (32.66)
    0x42,
    0x02,
    0xA3,
    0xD7
]);
