/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - unsigned byte

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 256
    // №256
    0x81,
    0x00,

    // type sensor 000 (10^0) 00000 (1 byte)
    0x00,

    //value
    0xff
]);
