/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - double

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 01001 (8 bytes)
    0x09,

    //value (-999.666)
    0xC0,
    0x8F,
    0x3D,
    0x53,
    0xF7,
    0xCE,
    0xD9,
    0x17
]);
