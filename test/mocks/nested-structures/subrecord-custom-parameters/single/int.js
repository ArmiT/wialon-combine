/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - unsigned int

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00010 (4 bytes)
    0x02,

    //value - 411254
    0x00,
    0x06,
    0x46,
    0x76
]);
