/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - unsigned long

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00011 (8 bytes)
    0x03,

    //value - 54510525546669
    0x00,
    0x00,
    0x31,
    0x93,
    0xB8,
    0x83,
    0x00,
    0xAD
]);
