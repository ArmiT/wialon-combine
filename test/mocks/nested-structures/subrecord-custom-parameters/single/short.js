/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - unsigned short

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00001 (2 bytes)
    0x01,

    //value -256
    0x01,
    0x00
]);
