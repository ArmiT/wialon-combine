/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// one parameter - signed long

module.exports = Buffer.from([

    // count
    0x01,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00111 (8 bytes)
    0x07,

    //value (-9223372036854775808)
    0x80,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00
]);
