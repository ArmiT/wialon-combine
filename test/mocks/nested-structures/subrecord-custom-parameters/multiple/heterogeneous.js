/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// nested structure - custom parameters
// three parameters - signed int, float, string

module.exports = Buffer.from([

    // count
    0x03,

    // custom param 1
    // №1
    0x01,

    // type sensor 000 (10^0) 00110 (4 bytes)
    0x06,

    //value (-1500000)
    0xFF,
    0xE9,
    0x1C,
    0xA0,

    // custom param 2
    // №2
    0x02,

    // type sensor 000 (10^0) 01000 (4 bytes)
    0x08,

    //value (32.66)
    0x42,
    0x02,
    0xA3,
    0xD7,

    // custom param 3
    // №3
    0x03,

    // type sensor 000 (10^0) 01010 (string 0 terminated)
    0x0A,

    //value (test)
    0x74,
    0x65,
    0x73,
    0x74,
    0x00
]);
