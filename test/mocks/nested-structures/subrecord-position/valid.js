/*jslint node */
/*eslint no-magic-numbers: "off", max-len: ["error", 80] */

"use strict";

// data package - single position sub-record
module.exports = Buffer.from([

    // POSITION DATA SUB-RECORD
    // lat - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // lon - 55000000
    0x03,
    0x47,
    0x3B,
    0xC0,

    // speed - 48 км/ч
    0x00,
    0x30,

    // course - 255 deg
    0x00,
    0xFF,

    // height - 32
    0x00,
    0x20,

    // satellites number - 14
    0x0E,

    // hdop - 110 (1.1)
    0x00,
    0x6E
]);
