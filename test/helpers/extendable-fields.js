/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {
    parseAsExtendableByte,
    parseAsExtendableShort,
    formatAsExtendableNumber
} = require("../../src/helpers/extendable-fields");

describe("helpers/extendable-fields#parse", function () {
    it(
        "byte - must correctly resolve single-byte values",
        function () {
            expect(parseAsExtendableByte.call({}, 0x79)).to.eql(true);
        }
    );

    it(
        "byte - must correctly resolve double-byte values - boundary",
        function () {
            const context = {};

            expect(parseAsExtendableByte.call(context, 0x80)).to.eql(false);
            expect(parseAsExtendableByte.call(context, 0xff)).to.eql(true);
        }
    );

    it(
        "byte - must correctly resolve double-byte values",
        function () {
            const context = {};

            expect(parseAsExtendableByte.call(context, 0x81)).to.eql(false);
            expect(parseAsExtendableByte.call(context, 0x00)).to.eql(true);
        }
    );

    it("must correctly clear context", function () {
        const context = {};

        expect(parseAsExtendableByte.call(context, 0x81)).to.eql(false);
        expect(parseAsExtendableByte.call(context, 0x00)).to.eql(true);

        expect(parseAsExtendableByte.call(context, 0x01)).to.eql(true);

        expect(parseAsExtendableByte.call(context, 0x81)).to.eql(false);
        expect(parseAsExtendableByte.call(context, 0x04)).to.eql(true);
    });

    it(
        "short - must correctly resolve non-extended value",
        function () {
            const context = {};

            expect(parseAsExtendableShort.call(context, 0x01)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0xff)).to.eql(true);
        }
    );

    it(
        "short - must correctly resolve extended value  - boundary",
        function () {
            const context = {};

            expect(parseAsExtendableShort.call(context, 0x80)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0xff)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0xff)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0xff)).to.eql(true);
        }
    );

    it(
        "short - must correctly resolve extended value",
        function () {
            const context = {};

            expect(parseAsExtendableShort.call(context, 0x81)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0x00)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0x0e)).to.eql(false);
            expect(parseAsExtendableShort.call(context, 0xff)).to.eql(true);
        }
    );
});

describe("helpers/extendable-fields#format", function () {
    it(
        "must correctly format as single-byte number",
        function () {
            expect(formatAsExtendableNumber([0x79])).to.eql(0x79);
        }
    );

    it(
        "must correctly format as double-byte number",
        function () {
            expect(formatAsExtendableNumber([0x81, 0x00])).to.eql(256);
        }
    );

    it(
        "must correctly format as four-byte number",
        function () {
            expect(formatAsExtendableNumber([0x80, 0x00, 0xff, 0xff]))
                .to
                .eql(65535);
        }
    );

    it(
        "must correctly format a max value",
        function () {
            expect(formatAsExtendableNumber([0xff, 0xff, 0xff, 0xff]))
                .to
                .eql(2147483647);
        }
    );
});
