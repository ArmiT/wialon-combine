/*jslint node */
/*eslint func-names: "off", no-magic-numbers: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {assert} = chai;

const calculateCrc16 = require("../../src/helpers/crc16");

const checkTable = [
    [[1], 0xC0C1],
    [[123], 0x2340],
    [[0x01, 0x02, 0x03, 0x80, 0xFF], 0xF8ED]
];

describe("helpers/crc16", function () {
    it("must correctly calculate checksum", function test() {
        return checkTable.map(function (item) {
            return assert.equal(calculateCrc16(item[0]), item[1]);
        });
    });
});
