/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const normalizeValues = require("../../src/helpers/values-normalizer");
const {v1, login, request} = require("../../src/wialon");

const {type} = login;

describe("helpers/values-normalizer", function () {
    it(
        "must correctly normalize login package",
        function () {
            expect(normalizeValues({
                type: 0,
                sequence: 1,
                payload: {
                    version: v1,
                    idLength: type.uint64,
                    pwdLength: type.uint64,
                    id: [0xFFFFFFFF, 0xFFFFFFFF],
                    pwd: [0xFFFFFFFF, 0xFFFFFFFF]
                }
            })).to.deep.include({
                payload: {
                    version: v1,
                    idLength: type.uint64,
                    pwdLength: type.uint64,
                    id: "18446744073709551615",
                    pwd: "18446744073709551615"
                }
            })
                .and.have.keys("type", "sequence");
        }
    );

    it(
        "must correctly normalize data package with custom parameters",
        function () {
            const result = normalizeValues({
                type: request.data,
                sequence: 1,
                payload: {
                    messages: [
                        {
                            time: 1462531045,
                            count: 1,
                            subrecords: [
                                {
                                    subrecordType: 0,
                                    subrecord: {
                                        paramsCount: 1,
                                        params: [
                                            {
                                                number: 1,
                                                powerOf10: 0,
                                                typeSensor: 7,
                                                value: [0xFFFFFFFF, 0x4D2FA200]
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            });

            const [message] = result.payload.messages;
            const [firstSubrecord] = message.subrecords;
            const [param] = firstSubrecord.subrecord.params;

            expect(param.value).to.eql("-3000000000");
        }
    );
});
