/*jslint node, bitwise */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");
const normalizeParameter = require(
    "../../src/helpers/custom-parameters-normalizer"
);

const {expect} = chai;

describe("helpers/custom-parameters-normalizer - no divider", function () {
    it("unsigned byte", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 0,
            value: 12
        })).to.include({
            value: 12
        });
    });

    it("unsigned short", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 1,
            value: 0x0100
        })).to.include({
            value: 256
        });
    });

    it("unsigned int", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 2,
            value: 0x00064676
        })).to.include({
            value: 411254
        });
    });

    it("signed byte", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 4,
            value: -21
        })).to.include({
            value: -21
        });
    });

    it("signed short", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 5,
            value: -300
        })).to.include({
            value: -300
        });
    });

    it("signed int", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 6,
            value: -1500000
        })).to.include({
            value: -1500000
        });
    });

    it("float", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 8,
            value: 32.66
        })).to.include({
            value: 32.66
        });
    });

    it("double", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 9,
            value: -999.666
        })).to.include({
            value: -999.666
        });
    });

    it("string", function () {
        expect(normalizeParameter({
            powerOf10: 0,
            typeSensor: 10,
            value: "test"
        })).to.include({
            value: "test"
        });
    });
});

describe(
    "helpers/custom-parameters-normalizer#long - no divider",
    function () {
        it("unsigned long", function () {
            expect(normalizeParameter({
                powerOf10: 0,
                typeSensor: 3,
                value: [0x00003193, 0xB88300AD]
            })).to.include({
                value: "54510525546669"
            });
        });

        it("unsigned max long", function () {
            expect(normalizeParameter({
                powerOf10: 0,
                typeSensor: 3,
                value: [0xFFFFFFFF, 0xFFFFFFFF]
            })).to.include({
                value: "18446744073709551615"
            });
        });

        it("signed long", function () {
            expect(normalizeParameter({
                powerOf10: 0,
                typeSensor: 7,
                value: [0xFFFFFFFF, 0x4D2FA200]
            })).to.include({
                value: "-3000000000"
            });
        });

        it("signed min long", function () {
            expect(normalizeParameter({
                powerOf10: 0,
                typeSensor: 7,
                value: [0x80000000, 0x00]
            })).to.include({
                value: "-9223372036854775808"
            });
        });

        it("signed max long", function () {
            expect(normalizeParameter({
                powerOf10: 0,
                typeSensor: 7,
                value: [0x7FFFFFFF, 0xFFFFFFFF]
            })).to.include({
                value: "9223372036854775807"
            });
        });
    }
);
