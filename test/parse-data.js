/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect, assert} = chai;

const {parse, wialon} = require("../src/index");

const {data} = wialon.request;
const {positionData, customParameters} = wialon.data.subrecordType;
const {type} = wialon.sensor;

const {
    singlePosition,
    doublePosition,
    doubleMessage,
    fewSubrecords,
    canWayL10
} = require("./mocks/request-data");

describe("parse/request/data", function () {
    it(
        "data request from a canway L10",
        function () {
            return parse(canWayL10)
                .then(function (result) {
                    return assert.isObject(result, "result is non an object");
                });
        }
    );

    it(
        "single message/single sub-record 'position'",
        function () {
            return parse(singlePosition)
                .then(function (result) {
                    return expect(result).to.deep.include({
                        type: data,
                        sequence: 1,
                        payload: {
                            messages: [
                                {
                                    time: 1462531045,
                                    count: 1,
                                    subrecords: [
                                        {
                                            subrecordType: positionData,
                                            subrecord: {
                                                lat: 55000000,
                                                lon: 55000000,
                                                speed: 48,
                                                course: 255,
                                                height: 32,
                                                satellites: 14,
                                                hdop: 110
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    })
                        .and.have.keys("start", "length", "crc");
                });
        }
    );

    it(
        "single message/two sub-records 'position'",
        function () {
            return parse(doublePosition)
                .then(function (result) {
                    const {payload} = result;
                    const {messages} = payload;

                    expect(messages.length).to.eql(1);

                    const [message] = messages;

                    expect(message).to.deep.include({
                        time: 1520766811,
                        count: 2
                    })
                        .and.have.keys("subrecords");

                    const {subrecords} = message;

                    expect(subrecords.length).to.eql(2);

                    const [firstSubrecord, secondSubrecord] = subrecords;

                    expect(firstSubrecord).to.eql({
                        subrecordType: positionData,
                        subrecord: {
                            lat: 55000000,
                            lon: 55000000,
                            speed: 48,
                            course: 255,
                            height: 32,
                            satellites: 14,
                            hdop: 110
                        }
                    });

                    return expect(secondSubrecord).to.eql({
                        subrecordType: positionData,
                        subrecord: {
                            lat: 56000000,
                            lon: 55000000,
                            speed: 60,
                            course: 45,
                            height: 261,
                            satellites: 9,
                            hdop: 90
                        }
                    });
                });
        }
    );

    it(
        "two messages/single sub-record 'position' per message",
        function () {
            return parse(doubleMessage)
                .then(function (result) {
                    const {payload} = result;
                    const {messages} = payload;

                    expect(messages.length).to.eql(2);

                    const [firstMessage, secondMessage] = messages;

                    expect(firstMessage).to.deep.include({
                        time: 1462531045,
                        count: 1
                    })
                        .and.have.keys("subrecords");

                    expect(secondMessage).to.deep.include({
                        time: 1520766811,
                        count: 1
                    })
                        .and.have.keys("subrecords");

                    expect(firstMessage.subrecords.length).to.eql(1);

                    return expect(secondMessage.subrecords.length).to.eql(1);
                });
        }
    );

    it(
        "one message/two sub-records",
        function () {
            return parse(fewSubrecords)
                .then(function (result) {
                    return expect(result).to.eql({
                        start: 0x2424,
                        type: data,
                        sequence: 1,
                        length: 119,
                        crc: 41953,
                        payload: {
                            messages: [
                                {
                                    time: 1462531045,
                                    count: 2,
                                    subrecords: [
                                        {
                                            subrecordType: positionData,
                                            subrecord: {
                                                lat: 55000000,
                                                lon: 55000000,
                                                speed: 48,
                                                course: 255,
                                                height: 32,
                                                satellites: 14,
                                                hdop: 110
                                            }
                                        },
                                        {
                                            subrecordType: customParameters,
                                            subrecord: {
                                                paramsCount: 15,
                                                params: [
                                                    {
                                                        number: 1,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint16,
                                                        value: 5454
                                                    },
                                                    {
                                                        number: 2,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint64,
                                                        value: "49646411459"
                                                    },
                                                    {
                                                        number: 3,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint64,
                                                        value: "54510525546669"
                                                    },
                                                    {
                                                        number: 4,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 411254
                                                    },
                                                    {
                                                        number: 5,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 758689
                                                    },
                                                    {
                                                        number: 6,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 223369
                                                    },
                                                    {
                                                        number: 7,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 542699
                                                    },
                                                    {
                                                        number: 8,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 12258899
                                                    },
                                                    {
                                                        number: 9,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 98982222
                                                    },
                                                    {
                                                        number: 10,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 8852699
                                                    },
                                                    {
                                                        number: 11,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 454444
                                                    },
                                                    {
                                                        number: 12,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 125548
                                                    },
                                                    {
                                                        number: 13,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 145548
                                                    },
                                                    {
                                                        number: 14,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint32,
                                                        value: 585241
                                                    },
                                                    {
                                                        number: 15,
                                                        powerOf10: 0,
                                                        typeSensor: type.uint16,
                                                        value: 21411
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    });
                });
        }
    );
});
