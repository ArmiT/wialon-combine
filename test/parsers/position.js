/*jslint node, bitwise */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {position} = require("../../src/parsers/");

const valid = require("../mocks/nested-structures/subrecord-position/valid");

describe("parsers/position", function () {
    it("must correctly parse valid structure", function () {
        const result = position.parse(valid);

        return expect(result).to.eql({
            lat: 55000000,
            lon: 55000000,
            speed: 48,
            course: 255,
            height: 32,
            satellites: 14,
            hdop: 110
        });
    });
});
