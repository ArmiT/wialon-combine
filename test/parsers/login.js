/*jslint node, bitwise */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {login} = require("../../src/parsers/");

const {wialon} = require("../../src/index");

const {type} = wialon.login;

const {
    nopwd,
    short,
    int,
    long,
    string,
    mixed,
    doubleVersion
} = require("../mocks/nested-structures/login");

describe("parsers/login", function () {
    it("must correctly parse structure LOGIN id=2 pwd=0", function () {
        expect(login.parse(nopwd)).to.eql({
            version: wialon.v1,
            idLength: type.uint16,
            pwdLength: type.omitted,
            id: 1
        });
    });

    it("must correctly parse structure LOGIN id=2 pwd=2", function () {
        expect(login.parse(short)).to.eql({
            version: wialon.v1,
            idLength: type.uint16,
            pwdLength: type.uint16,
            id: 1,
            pwd: 7777
        });
    });

    it("must correctly parse structure LOGIN id=4 pwd=4", function () {
        expect(login.parse(int)).to.eql({
            version: wialon.v1,
            idLength: type.uint32,
            pwdLength: type.uint32,
            id: 0x01FFFFFF,
            pwd: 0x01020304
        });
    });

    it("must correctly parse structure LOGIN id=8 pwd=8", function () {
        expect(login.parse(long)).to.eql({
            version: wialon.v1,
            idLength: type.uint64,
            pwdLength: type.uint64,
            id: [0x0001429a, 0x366b8e79],
            pwd: [0x0001429a, 0x366b8e79]
        });
    });

    it(
        "must correctly parse structure LOGIN id=string pwd=string",
        function () {
            expect(login.parse(string)).to.eql({
                version: wialon.v1,
                idLength: type.string,
                pwdLength: type.string,
                id: "123",
                pwd: "test"
            });
        }
    );

    it(
        "must correctly parse structure LOGIN id=8 pwd=string",
        function () {
            expect(login.parse(mixed)).to.eql({
                version: wialon.v1,
                idLength: type.uint64,
                pwdLength: type.string,
                id: [0x0001429a, 0x366b8e79],
                pwd: "test"
            });
        }
    );

    it("must correctly parse structure LOGIN - double version", function () {
        expect(login.parse(doubleVersion)).to.eql({
            version: 256,
            idLength: type.uint16,
            pwdLength: type.uint16,
            id: 1,
            pwd: 7777
        });
    });
});
