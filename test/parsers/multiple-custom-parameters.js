/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {customParameters} = require("../../src/parsers/");

const {sensor} = require("../../src/wialon");

const {type} = sensor;

const {
    homogeneous,
    heterogeneous
} = require(
    "../mocks/nested-structures/subrecord-custom-parameters/multiple"
);

describe("parsers/custom-parameters/multiple", function () {
    it("must correctly parse homogeneous sub-records", function () {
        const result = customParameters.parse(homogeneous);

        expect(result).to.have.keys("paramsCount", "params");

        const {params, paramsCount} = result;

        expect(params).to.have.lengthOf(paramsCount);

        return expect(params).to.eql([
            {
                number: 1,
                powerOf10: 0,
                typeSensor: type.uint32,
                value: 411254
            },
            {
                number: 2,
                powerOf10: 0,
                typeSensor: type.uint32,
                value: 255
            }
        ]);
    });

    it("must correctly parse heterogeneous sub-records", function () {
        const result = customParameters.parse(heterogeneous);

        expect(result).to.have.keys("paramsCount", "params");

        const {params, paramsCount} = result;

        expect(params).to.have.lengthOf(paramsCount);

        const [firstParam, secondParam, thirdParam] = params;

        expect(firstParam).to.eql({
            number: 1,
            powerOf10: 0,
            typeSensor: type.int32,
            value: -1500000
        });

        expect(thirdParam).to.eql({
            number: 3,
            powerOf10: 0,
            typeSensor: type.string,
            value: "test"
        });

        expect(secondParam).to.include({
            number: 2,
            powerOf10: 0,
            typeSensor: type.float
        })
            .and.have.keys("value");

        return expect(
            parseFloat(
                secondParam.value.toFixed(2)
            )
        ).to.eql(32.66);
    });
});
