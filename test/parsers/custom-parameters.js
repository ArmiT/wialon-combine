/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {customParameters} = require("../../src/parsers/");

const {sensor} = require("../../src/wialon");

const {type} = sensor;

const {
    byte,
    short,
    int,
    long,
    maxLong,
    signedByte,
    signedShort,
    signedInt,
    signedLong,
    signedMinLong,
    signedMaxLong,
    float,
    double,
    string,
    extendedNumber
} = require(
    "../mocks/nested-structures/subrecord-custom-parameters/single"
);

describe("parsers/custom-parameters/single#unsigned", function () {
    it("must correctly parse byte", function () {
        const result = customParameters.parse(byte);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.uint8,
                    value: 255
                }
            ]
        });
    });

    it("must correctly parse short", function () {
        const result = customParameters.parse(short);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.uint16,
                    value: 256
                }
            ]
        });
    });

    it("must correctly parse int", function () {
        const result = customParameters.parse(int);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.uint32,
                    value: 411254
                }
            ]
        });
    });

    it("must correctly parse long", function () {
        const result = customParameters.parse(long);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.uint64,
                    value: [0x00003193, 0xB88300AD]
                }
            ]
        });
    });

    it("must correctly parse max long value", function () {
        const result = customParameters.parse(maxLong);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.uint64,
                    value: [0xFFFFFFFF, 0xFFFFFFFF]
                }
            ]
        });
    });

    it("must correctly parse float", function () {
        const result = customParameters.parse(float);

        expect(result).to.include({
            paramsCount: 1
        })
            .and.have.keys("params");

        const {params} = result;

        expect(params).to.have.lengthOf(1);

        const [param] = params;

        expect(param).to.include({
            number: 1,
            powerOf10: 0,
            typeSensor: type.float
        })
            .and.have.keys("value");

        return expect(parseFloat(param.value.toFixed(2))).to.eql(32.66);
    });

    it("must correctly parse double", function () {
        const result = customParameters.parse(double);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.double,
                    value: -999.666
                }
            ]
        });
    });

    it("must correctly parse string", function () {
        const result = customParameters.parse(string);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.string,
                    value: "test"
                }
            ]
        });
    });

    it("must correctly parse extended number", function () {
        const result = customParameters.parse(extendedNumber);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 256,
                    powerOf10: 0,
                    typeSensor: type.uint8,
                    value: 255
                }
            ]
        });
    });
});

describe("parsers/custom-parameters/single#signed", function () {
    it("must correctly parse signed byte", function () {
        const result = customParameters.parse(signedByte);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int8,
                    value: -21
                }
            ]
        });
    });

    it("must correctly parse signed short", function () {
        const result = customParameters.parse(signedShort);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int16,
                    value: -300
                }
            ]
        });
    });

    it("must correctly parse signed int", function () {
        const result = customParameters.parse(signedInt);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int32,
                    value: -1500000
                }
            ]
        });
    });

    it("must correctly parse signed long", function () {
        const result = customParameters.parse(signedLong);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int64,
                    value: [0xFFFFFFFF, 0x4D2FA200]
                }
            ]
        });
    });

    it("must correctly parse min signed long value", function () {
        const result = customParameters.parse(signedMinLong);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int64,
                    value: [0x80000000, 0x00]
                }
            ]
        });
    });

    it("must correctly parse max signed long value", function () {
        const result = customParameters.parse(signedMaxLong);

        return expect(result).to.eql({
            paramsCount: 1,
            params: [
                {
                    number: 1,
                    powerOf10: 0,
                    typeSensor: type.int64,
                    value: [0x7FFFFFFF, 0xFFFFFFFF]
                }
            ]
        });
    });
});
