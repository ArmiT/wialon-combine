/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {parse, wialon} = require("../src/index");

const {data} = wialon.request;
const {positionData, customParameters} = wialon.data.subrecordType;
const {type} = wialon.sensor;

const {complex} = require("./mocks/request-data");

describe("parse/request/complex data", function () {
    it("two messages, heterogeneous sub-records", function () {
        function check(result) {
            return expect(result).to.eql({
                start: 0x2424,
                type: data,
                sequence: 1,
                length: 89,
                crc: 0x7D67,
                payload: {
                    messages: [
                        {
                            time: 1462531045,
                            count: 2,
                            subrecords: [
                                {
                                    subrecordType: positionData,
                                    subrecord: {
                                        lat: 56000000,
                                        lon: 55000000,
                                        speed: 60,
                                        course: 45,
                                        height: 261,
                                        satellites: 9,
                                        hdop: 90
                                    }
                                },
                                {
                                    subrecordType: customParameters,
                                    subrecord: {
                                        paramsCount: 2,
                                        params: [
                                            {
                                                number: 1,
                                                powerOf10: 0,
                                                typeSensor: type.uint64,
                                                value: "54510525546669"
                                            },
                                            {
                                                number: 2,
                                                powerOf10: 1,
                                                typeSensor: type.uint32,
                                                value: 41125.4
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            time: 1520766811,
                            count: 2,
                            subrecords: [
                                {
                                    subrecordType: positionData,
                                    subrecord: {
                                        lat: 55000000,
                                        lon: 55000000,
                                        speed: 48,
                                        course: 255,
                                        height: 32,
                                        satellites: 14,
                                        hdop: 110
                                    }
                                },
                                {
                                    subrecordType: customParameters,
                                    subrecord: {
                                        paramsCount: 3,
                                        params: [
                                            {
                                                number: 1,
                                                powerOf10: 0,
                                                typeSensor: type.int32,
                                                value: -1500000
                                            },
                                            {
                                                number: 2,
                                                powerOf10: 6,
                                                typeSensor: type.int64,
                                                value: "9223372036854.775807"
                                            },
                                            {
                                                number: 3,
                                                powerOf10: 0,
                                                typeSensor: type.string,
                                                value: "test"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            });
        }

        return parse(complex)
            .then(check);
    });
});
