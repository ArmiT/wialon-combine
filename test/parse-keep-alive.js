/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {parse, wialon} = require("../src/index");

const {keepAlive} = wialon.request;

const {valid} = require("./mocks/request-keep-alive");

describe("parse/request/keep-alive", function () {
    it("must correctly parse package keep-alive", function () {
        return parse(valid)
            .then(function (result) {
                return expect(result).to.eql({
                    start: wialon.startCondition,
                    type: keepAlive,
                    sequence: 1
                });
            });
    });
});
