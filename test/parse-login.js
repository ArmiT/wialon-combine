/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {parse, wialon} = require("../src/index");

const {v1, request, login} = wialon;
const {type} = login;

const {
    short,
    nopwd,
    int,
    long,
    maxLong,
    string,
    mixed
} = require("./mocks/request-login");

describe("parse/request/login", function () {
    it("must correctly parse package id=2 pwd=0", function () {
        return parse(nopwd)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.uint16,
                        pwdLength: type.omitted,
                        id: 1
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse package id=2 pwd=2", function () {
        return parse(short)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.uint16,
                        pwdLength: type.uint16,
                        id: 1,
                        pwd: 7777
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse package id=4 pwd=4", function () {
        return parse(int)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.uint32,
                        pwdLength: type.uint32,
                        id: 0x01FFFFFF,
                        pwd: 0x01020304
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse package id=8 pwd=8", function () {
        return parse(long)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.uint64,
                        pwdLength: type.uint64,
                        id: "72623859790382856",
                        pwd: "578437695752307201"
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse package with max long values", function () {
        return parse(maxLong)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.uint64,
                        pwdLength: type.uint64,
                        id: "18446744073709551615",
                        pwd: "18446744073709551615"
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse package id=string pwd=string", function () {
        return parse(string)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: v1,
                        idLength: type.string,
                        pwdLength: type.string,
                        id: "123",
                        pwd: "test"
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });

    it("must correctly parse mixed package id=4 pwd=string", function () {
        return parse(mixed)
            .then(function (result) {
                return expect(result).to.deep.include({
                    type: request.login,
                    sequence: 1,
                    payload: {
                        version: wialon.v1,
                        idLength: type.uint32,
                        pwdLength: type.string,
                        id: 0x01020304,
                        pwd: "test"
                    }
                })
                    .and.have.keys("start", "length", "crc");
            });
    });
});
