/*jslint node, bitwise, white: true */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");
const chaiAsPromised = require("chai-as-promised");

const {expect, assert} = chai;

const {parse, wialon} = require("../src/index");

const {keepAlive} = wialon.request;
const {response} = wialon;

const {
    valid,
    incorrectStart,
    incorrectChecksum
} = require("./mocks/request-head");

chai.use(chaiAsPromised);

describe("parse/header", function () {
    it("must correctly parse valid head", function () {
        return parse(valid)
            .then(function (result) {
                return expect(result).to.eql({
                    start: wialon.startCondition,
                    type: keepAlive,
                    sequence: 1
                });
            });
    });

    it("must trigger error for invalid start number", function () {
        return assert.isRejected(
            parse(incorrectStart),
            Error,
            "Incorrect start condition"
        )
            .then(function (error) {
                expect(error).to.have.keys("request", "type");
                expect(error.request).to.include.keys("sequence");

                return assert.equal(error.type, response.error);
            });
    });

    it("must trigger error for invalid checksum", function () {
        return assert.isRejected(
            parse(incorrectChecksum),
            Error,
            "Incorrect checksum"
        )
            .then(function (error) {
                expect(error).to.have.keys("request", "type");
                expect(error.request).to.include.keys("sequence");

                return assert.equal(error.type, response.crcError);
            });
    });

    it("must try parse sequence from broken request", function () {
        return assert.isRejected(
            parse(Buffer.from([0x24, 0x24, 0x00, 0x00, 0x01])),
            Error,
            "Parse error"
        )
            .then(function (error) {
                expect(error).to.have.keys("type", "request");
                expect(error.request).to.include.keys("sequence");
                expect(error.request.sequence).to.eql(1);

                return assert.equal(error.type, response.error);
            });
    });
});
