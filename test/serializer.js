/*jslint node, bitwise */
/*eslint func-names: "off", no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const chai = require("chai");
const {describe, it} = require("mocha");

const {expect} = chai;

const {serializer, wialon} = require("../src/index");

const {
    makeSuccessfulResponse,
    makeAuthErrorResponse,
    makePwdErrorResponse,
    makeFailureResponse,
    makeCrcErrorResponse
} = serializer;

const {
    success,
    authError,
    wrongPwd,
    error,
    crcError
} = wialon.response;

describe("serialize", function () {
    it("must correctly serialize successful response", function () {
        return expect(makeSuccessfulResponse(1)).to.eql([
            0x40,
            0x40,
            success,
            0x00,
            0x01
        ]);
    });

    it("must correctly serialize auth error response", function () {
        return expect(makeAuthErrorResponse(1)).to.eql([
            0x40,
            0x40,
            authError,
            0x00,
            0x01
        ]);
    });

    it("must correctly serialize pwd error response", function () {
        return expect(makePwdErrorResponse(1)).to.eql([
            0x40,
            0x40,
            wrongPwd,
            0x00,
            0x01
        ]);
    });

    it("must correctly serialize failure response", function () {
        return expect(makeFailureResponse(1)).to.eql([
            0x40,
            0x40,
            error,
            0x00,
            0x01
        ]);
    });

    it("must correctly serialize crc error response", function () {
        return expect(makeCrcErrorResponse(1)).to.eql([
            0x40,
            0x40,
            crcError,
            0x00,
            0x01
        ]);
    });
});
