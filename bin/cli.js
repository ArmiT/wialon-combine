#!/usr/bin/env node

"use strict";

const program = require("commander");
const {version} = require("../package.json");

program.version(version, "-v, --version");

program.command("test")
    .description("test descr")
    .option("-s", "test option")
    .action(function processTest(cmd, options) {
        //console.log(`${cmd}, ${options}`);
    });

program.parse(process.argv);
