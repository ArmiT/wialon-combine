/*jslint node */

"use strict";

const wialon = require("./wialon");
const parse = require("./parser");
const serializer = require("./serializer");
const errors = require("./errors");

module.exports = Object.freeze({
    wialon,
    parse,
    serializer,
    errors
});
