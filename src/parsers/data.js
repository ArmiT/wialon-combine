/*jslint node, this, white: true */
/*eslint fp/no-this: "off", no-invalid-this: "off" */

"use strict";

const {Parser} = require("binary-parser");

const position = require("./position");
const customParameters = require("./customParameters");

function getSubrecordsCount() {
    return this.count;
}

function getMessagesLength(context) {
    const {length} = context;

    return length;
}

module.exports = Parser.start()
    .endianess("big")
    .array("messages", {
        type: Parser.start()
            .endianess("big")
            .uint32("time")
            .uint8("count")
            .array("subrecords", {
                type: Parser.start()
                    .endianess("big")

                    // subrecord type - extendable byte,
                    // but canway don't use this possibility
                    .uint8("subrecordType")
                    .choice("subrecord", {
                        tag: "subrecordType",
                        choices: {
                            "0x00": customParameters,
                            "0x01": position
                        }
                    }),
                length: getSubrecordsCount
            }),
        lengthInBytes: getMessagesLength
    });
