/*jslint node, this, white: true */

"use strict";

const {Parser} = require("binary-parser");

module.exports = Parser.start()
    .endianess("big")
    .uint32("lat")
    .uint32("lon")
    .uint16("speed")
    .uint16("course")
    .uint16("height")
    .uint8("satellites")
    .uint16("hdop");
