/*jslint node, this, white: true */
/*eslint fp/no-this: "off", no-invalid-this: "off" */

"use strict";

const {Parser} = require("binary-parser");

const {
    parseAsExtendableByte,
    formatAsExtendableNumber
} = require("../helpers/extendable-fields");

function getParamsCount() {
    return this.paramsCount;
}

module.exports = Parser.start()
    .endianess("big")
    .array(
        "paramsCount",
        {
            type: "uint8",
            readUntil: parseAsExtendableByte,
            formatter: formatAsExtendableNumber
        }
    )
    .array("params", {
        type: Parser.start()
            .endianess("big")
            .array(
                "number",
                {
                    type: "uint8",
                    readUntil: parseAsExtendableByte,
                    formatter: formatAsExtendableNumber
                }
            )
            .bit3("powerOf10")
            .bit5("typeSensor")
            .choice(null, {
                tag: "typeSensor",
                choices: {
                    "0": Parser.start()
                        .endianess("big")
                        .uint8("value"),
                    "1": Parser.start()
                        .endianess("big")
                        .uint16("value"),
                    "2": Parser.start()
                        .endianess("big")
                        .uint32("value"),
                    "3": Parser.start().array(
                        "value",
                        {
                            type: "uint32be",
                            length: 2
                        }
                    ),
                    "4": Parser.start()
                        .endianess("big")
                        .int8("value"),
                    "5": Parser.start()
                        .endianess("big")
                        .int16("value"),
                    "6": Parser.start()
                        .endianess("big")
                        .int32("value"),
                    "7": Parser.start().array(
                        "value",
                        {
                            type: "uint32be",
                            length: 2
                        }
                    ),
                    "8": Parser.start()
                        .endianess("big")
                        .floatbe("value"),
                    "9": Parser.start()
                        .endianess("big")
                        .doublebe("value"),
                    "10": Parser.start()
                        .endianess("big")
                        .string("value", {
                            encoding: "ascii",
                            zeroTerminated: true
                        })
                }
            }),
        length: getParamsCount
    });
