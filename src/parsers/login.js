/*jslint node, white: true */

"use strict";

const {Parser} = require("binary-parser");
const {
    parseAsExtendableByte,
    formatAsExtendableNumber
} = require("../helpers/extendable-fields");

module.exports = Parser.start()
    .endianess("big")
    .array(
        "version",
        {
            type: "uint8",
            readUntil: parseAsExtendableByte,
            formatter: formatAsExtendableNumber
        }
    )
    .bit4("idLength")
    .bit4("pwdLength")
    .choice(
        null,
        {
            tag: "idLength",
            choices: {
                "0x01": Parser.start().endianess("big").uint16("id"),
                "0x02": Parser.start().endianess("big").uint32("id"),
                "0x03": Parser.start().array(
                    "id",
                    {
                        type: "uint32be",
                        length: 2
                    }
                ),
                "0x04": Parser.start().endianess("big").string("id", {
                    encoding: "ascii",
                    zeroTerminated: true
                })
            }
        }
    )
    .choice(
        null,
        {
            tag: "pwdLength",
            choices: {
                "0x00": Parser.start(),
                "0x01": Parser.start().endianess("big").uint16("pwd"),
                "0x02": Parser.start().endianess("big").uint32("pwd"),
                "0x03": Parser.start().array(
                    "pwd",
                    {
                        type: "uint32be",
                        length: 2
                    }
                ),
                "0x04": Parser.start().endianess("big").string("pwd", {
                    encoding: "ascii",
                    zeroTerminated: true
                })
            }
        }
    );
