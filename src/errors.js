/*jslint node */

"use strict";

const {response} = require("./wialon");

const {error, crcError} = response;

module.exports = {
    makeIncorrectStartConditionError(request) {
        // eslint-disable-next-line fp/no-mutation, fp/no-mutating-assign
        return Object.assign(
            new Error("Incorrect start condition"),
            {
                type: error,
                request
            }
        );
    },
    makeParseError(request) {
        // eslint-disable-next-line fp/no-mutation, fp/no-mutating-assign
        return Object.assign(
            new Error("Parse error"),
            {
                type: error,
                request
            }
        );
    },
    makeIncorrectChecksumError(request) {
        // eslint-disable-next-line fp/no-mutation, fp/no-mutating-assign
        return Object.assign(
            new Error("Incorrect checksum"),
            {
                type: crcError,
                request
            }
        );
    }
};
