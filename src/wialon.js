/*jslint node */
/*eslint no-magic-numbers: "off" */

"use strict";

module.exports = Object.freeze({
    v1: 1,
    startCondition: 0x2424,
    responseStartCondition: [0x40, 0x40],
    request: {
        login: 0x00,
        data: 0x01,
        keepAlive: 0x02
    },
    response: {
        success: 0x00,
        authError: 0x01,
        wrongPwd: 0x02,
        error: 0x03,
        crcError: 0x04,
        command: 0xFF
    },
    login: {
        type: {
            omitted: 0x00,
            uint16: 0x01,
            uint32: 0x02,
            uint64: 0x03,
            string: 0x04
        }
    },
    data: {
        subrecordType: {
            customParameters: 0x00,
            positionData: 0x01
        }
    },
    sensor: {
        type: {
            uint8: 0,
            uint16: 0x01,
            uint32: 0x02,
            uint64: 0x03,
            int8: 0x04,
            int16: 0x05,
            int32: 0x06,
            int64: 0x07,
            float: 0x08,
            double: 0x09,
            string: 0x0A
        }
    }
});
