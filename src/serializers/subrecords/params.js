/*jslint node, bitwise, white: true */
/*eslint no-magic-numbers: "off", no-bitwise: "off", max-statements: 0 */

"use strict";

const Joi = require("@hapi/joi");
const R = require("ramda");

// FYI see p.6 https://gurtam.com/hw/files/Wialon%20Combine_v1.0.4.pdf
const paramType = {
    uint8: 0,
    uint16: 1,
    uint32: 2,
    uint64: 3,
    int8: 4,
    int16: 5,
    int32: 6,
    int64: 7,
    float: 8,
    double: 9,
    string: 10
};

const sizeBytes = {
    [paramType.uint8]: 1,
    [paramType.uint16]: 2,
    [paramType.uint32]: 4,
    [paramType.uint64]: 8,
    [paramType.int8]: 1,
    [paramType.int16]: 2,
    [paramType.int32]: 4,
    [paramType.int64]: 8,
    [paramType.float]: 4,
    [paramType.double]: 8
};

const schema = Joi.array().items(Joi.object({
    num: Joi.number().integer().positive().required(),
    type: Joi.number().integer().valid(...Object.values(paramType)).required(),
    value: Joi.alternatives().try([
        Joi.number(),
        Joi.string()
    ]),
    denominatorOrder: Joi.number().integer().min(0).optional().default(0)
}));

function calcRecordSize({num, type, value}) {
    const numSize = num > 128 ? 2 : 1;
    const typeSize = 1;
    const valueSize = type != paramType.string ? 
        sizeBytes[type] : 
        value.length + 1;

    return numSize + typeSize + valueSize;
}

function makeParameters(proposed) {
    let params = [];

    function build() {
        return R.pipe(
            (params) => schema.validate(params),
            R.unless(R.propEq("error", null), function throwError({error}) {
                throw error;
            }),
            R.prop("value"),
            R.map(function encodeParam(param) {
                return R.pipe(
                    R.tap(console.log),
                    calcRecordSize,
                    function allocateBuffer(size) {
                        return new ArrayBuffer(size);
                    },
                    function fill(buffer) {
                        const record = new DataView(buffer);
                        record.set

                    }
                )(param);
            })
        )
        (params);

        // for each param calculate final size
        // - get num size
        // - get value size based on type or own length
        // - +1 for a type
        // create buffer
        // pack params
        // append filled buffer to the array
        // flatten array
        // preppend size

    }

    const builder = {
        addParameter({num, type, value, denominatorOrder}) {
            params.push({
                num, 
                type, 
                value, 
                denominatorOrder
            });

            return builder;
        },
        addUint8(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.uint8, 
                value, 
                denominatorOrder
            });
        },
        addUint16(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.uint16, 
                value, 
                denominatorOrder
            });
        },
        addUnit32(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.uint32, 
                value, 
                denominatorOrder
            });
        },
        addUint64(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.uint64, 
                value, 
                denominatorOrder
            });
        },
        addInt8(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.int8, 
                value, 
                denominatorOrder
            });
        },
        addInt16(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.int16, 
                value, 
                denominatorOrder
            });
        },
        addInt32(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.int32, 
                value, 
                denominatorOrder
            });
        },
        addInt64(num, value, denominatorOrder){
            return builder.addParameter({
                num, 
                type: 
                paramType.int64, 
                value, 
                denominatorOrder
            });
        },
        addFloat(num, value){
            return builder.addParameter({
                num, 
                type: 
                paramType.float, 
                value, 
                denominatorOrder
            });
        },
        addDouble(num, value){
            return builder.addParameter({
                num, 
                type: 
                paramType.double, 
                value, 
                denominatorOrder
            });
        },
        addString(num, value){
            return builder.addParameter({
                num, 
                type: 
                paramType.string, 
                value, 
                denominatorOrder
            });
        },
        build
    };

    return builder;
}

module.exports = Object.freeze({
    makeParameters,
    paramType,
    calcRecordSize
});
