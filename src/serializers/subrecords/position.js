/*jslint node, bitwise, white: true */
/*eslint no-magic-numbers: "off", no-bitwise: "off", max-statements: 0 */

"use strict";

const Joi = require("@hapi/joi");

// subrecord type "position"
// structure
// bytes:   4   4   2     2      2      1    2
// signed:  s   s   u     u      s      u    u
// section: Lat Lon Speed Course Height Sats Hdop
const minLatitude = -90.000000;
const maxLatitude = 90.000000;
const minLongitude = -180.000000;
const maxLongitude = 180.000000;
const maxSpeed = 65535;
const maxDegree = 360;
const minMASL = -32768;
const maxMASL = 32767;
const maxSatellitesCount = 255;
const maxHDOP = 100;

// lat * 1000000, lon * 1000000
// see p.7 off https://gurtam.com/hw/files/Wialon%20Combine_v1.0.4.pdf
const geoMultiplier = 1000000;

// normal HDOP * 100
// FYI see p.7 off https://gurtam.com/hw/files/Wialon%20Combine_v1.0.4.pdf
const hdopMultiplier = 100;

const subrecordSizeBytes = 17;

const offset = {
    lat: 0,
    lon: 4,
    speed: 8,
    course: 10,
    height: 12,
    sats: 14,
    hdop: 15
};

const schema = Joi.object({
    lat: Joi.number()
        .integer()
        .min(minLatitude * geoMultiplier)
        .max(maxLatitude * geoMultiplier)
        .required(),
    lon: Joi
        .number()
        .integer()
        .min(minLongitude * geoMultiplier)
        .max(maxLongitude * geoMultiplier)
        .required(),
    speed: Joi.number().integer().min(0).max(maxSpeed).required(),
    course: Joi.number().integer().min(0).max(maxDegree).required(),
    height: Joi.number().integer().min(minMASL).max(maxMASL).required(),
    sats: Joi.number().integer().min(0).max(maxSatellitesCount).required(),
    hdop: Joi
        .number()
        .integer()
        .min(0 * hdopMultiplier)
        .max(maxHDOP * hdopMultiplier)
        .required()
});

function makePosition(proposed) {
    const stub = Object.assign(
        {
            lat: 0,
            lon: 0,
            speed: 0,
            course: 0,
            height: 0,
            sats: 0,
            hdop: 0
        },
        proposed
    );

    function build() {
        const data = new ArrayBuffer(subrecordSizeBytes);

        const {error, value} = schema.validate(stub);

        if (error) {
            throw error;
        }

        const record = new DataView(data);

        record.setUint32(offset.lat, value.lat);
        record.setInt32(offset.lon, value.lon);
        record.setUint16(offset.speed, value.speed);
        record.setUint16(offset.course, value.course);
        record.setInt16(offset.height, value.height);
        record.setUint8(offset.sats, value.sats);
        record.setUint16(offset.hdop, value.hdop);

        return Buffer.from(data);
    }

    const builder = {
        set(attr, val) {
            // eslint-disable-next-line fp/no-mutation, fp/no-mutating-assign
            stub[attr] = val;

            return builder;
        },
        geo(lat, lon) {
            return builder.lat(lat)
                .lon(lon);
        },
        lat(lat) {
            return builder.set("lat", lat * geoMultiplier);
        },
        lon(lon) {
            return builder.set("lon", lon * geoMultiplier);
        },
        speed(speed) {
            return builder.set("speed", speed);
        },
        course(course) {
            return builder.set("course", course);
        },
        height(height) {
            return builder.set("height", height);
        },
        sats(sats) {
            return builder.set("sats", sats);
        },
        hdop(hdop) {
            return builder.set("hdop", hdop * hdopMultiplier);
        },
        build
    };

    return builder;
}

module.exports = makePosition;
