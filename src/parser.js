/*jslint node, white: true */

"use strict";

const Bluebird = require("bluebird");
const R = require("ramda");
const {Parser} = require("binary-parser");

const calculateCrc = require("./helpers/crc16");
const {startCondition, request} = require("./wialon");

const {keepAlive} = request;

const {
    makeIncorrectStartConditionError,
    makeParseError,
    makeIncorrectChecksumError
} = require("./errors");

const {
    parseAsExtendableByte,
    parseAsExtendableShort,
    formatAsExtendableNumber
} = require("./helpers/extendable-fields");

const normalizeValues = require("./helpers/values-normalizer");

const {login, data} = require("./parsers");

module.exports = function makeParser(incomingBytes) {
    const crcStartPosition = -2;

    function makeHeadParser() {
        return Parser.start()
            .endianess("big")
            .uint16("start")
            .array(
                "type",
                {
                    type: "uint8",
                    readUntil: parseAsExtendableByte,
                    formatter: formatAsExtendableNumber
                }
            )
            .uint16("sequence");
    }

    function makeBodyParser(nestedParser) {
        return Parser.start()
            .endianess("big")
            .array(
                "length",
                {
                    type: "uint8",
                    readUntil: parseAsExtendableShort,
                    formatter: formatAsExtendableNumber
                }
            )
            .nest("payload", {type: nestedParser})
            .uint16("crc");
    }

    return new Bluebird(function parse(resolve, reject) {
        const frame = new Parser()
            .nest(null, {type: makeHeadParser()})
            .choice(null, {
                tag: "type",
                choices: {
                    "0x00": makeBodyParser(login),
                    "0x01": makeBodyParser(data),
                    "0x02": Parser.start()
                }
            });

        // eslint-disable-next-line fp/no-let
        let result = {};

        try {
            // eslint-disable-next-line fp/no-mutation, fp/no-mutating-assign
            result = frame.parse(incomingBytes);
        } catch (ignore) {
            return reject(
                makeParseError(
                    makeHeadParser()
                        .parse(incomingBytes)
                )
            );
        }

        R.pipe(
            R.unless(
                R.propEq("start", startCondition),
                (structure) => reject(
                    makeIncorrectStartConditionError(structure)
                )
            ),
            R.when(
                R.propEq("type", keepAlive),
                resolve
            ),
            R.unless(
                R.propEq(
                    "crc",
                    calculateCrc(incomingBytes.slice(0, crcStartPosition))
                ),
                (structure) => reject(
                    makeIncorrectChecksumError(structure)
                )
            ),
            normalizeValues,
            resolve
        )(result);
    });
};
