/*jslint node */

"use strict";

const R = require("ramda");
const Long = require("long");

module.exports = R.curry(function makeBigNumber(isUnsigned, arrayOfBytes) {
    return (
        new Long(
            R.last(arrayOfBytes),
            R.head(arrayOfBytes),
            isUnsigned
        )
    ).toString();
});
