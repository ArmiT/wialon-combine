/*jslint node */

/*property
    __, both, includes, divide, evolve, exports, float, gt, ifElse, int64, lt,
    pipe, pow, powerOf10, prop, propEq, sensor, type, uint64, value, when
*/

"use strict";

const R = require("ramda");
const makeBigNumber = require("./big-numbers-maker");
const bigDecimal = require("js-big-decimal");
const {sensor} = require("../wialon");

const {
    uint64,
    int64,
    float
} = sensor.type;

const basis = 10;

const processLongValues = R.pipe(
    R.when(
        R.propEq("typeSensor", uint64),
        R.evolve({
            value: makeBigNumber(true)
        })
    ),
    R.when(
        R.propEq("typeSensor", int64),
        R.evolve({
            value: makeBigNumber(false)
        })
    )
);

const applyDivider = R.when(
    R.both(
        R.pipe(
            R.prop("typeSensor"),
            R.gt(float)
        ),
        R.pipe(
            R.prop("powerOf10"),
            R.lt(0)
        )
    ),
    R.ifElse(
        R.pipe(
            R.prop("typeSensor"),
            R.includes(R.__, [uint64, int64])
        ),
        (param) => R.evolve({
            value: (value) => bigDecimal.divide(
                value,
                Math.pow(basis, param.powerOf10),
                param.powerOf10
            )
        })(param),
        (param) => R.evolve({
            value: R.divide(R.__, Math.pow(basis, param.powerOf10))
        })(param)
    )
);

module.exports = R.pipe(
    processLongValues,
    applyDivider
);
