/*jslint node, white: true */

"use strict";

const R = require("ramda");

const {request} = require("../wialon");

const {login, data, keepAlive} = request;

const normalizeCustomParamValue = require("./custom-parameters-normalizer");
const makeBigNumber = require("./big-numbers-maker");

const convertToBigNumberIfNecessary = R.when(
    Array.isArray,
    makeBigNumber(true)
);

module.exports = R.cond([
    [
        R.propEq("type", login),
        R.evolve({
            payload: R.evolve({
                id: convertToBigNumberIfNecessary,
                pwd: convertToBigNumberIfNecessary
            })
        })
    ],
    [
        R.propEq("type", data),
        R.evolve({
            payload: R.evolve({
                messages: R.map(
                    R.evolve({
                        subrecords: R.map(
                            R.evolve({
                                subrecord: R.evolve({
                                    params: R.map(normalizeCustomParamValue)
                                })
                            })
                        )
                    })
                )
            })
        })
    ],
    [
        R.propEq("type", keepAlive),
        R.identity
    ]
]);
