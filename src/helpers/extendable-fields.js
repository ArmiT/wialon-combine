/*jslint node, bitwise, this, white: true */
/*eslint fp/no-this: "off", no-invalid-this: "off", no-bitwise: "off"  */

"use strict";

function parseAsExtendableByte(item) {
    const minLength = 1;
    const maxLength = 2;

    if (this.callsCount === undefined) {
        const narrowBoundary = 127;

        // eslint-disable-next-line fp/no-mutation
        this.callsCount = (item > narrowBoundary
            ? maxLength
            : minLength);
    }

    // eslint-disable-next-line fp/no-mutation
    this.callsCount -= 1;

    if (this.callsCount <= 0) {
        // eslint-disable-next-line fp/no-delete
        delete this.callsCount;

        return true;
    }

    return false;
}

function parseAsExtendableShort(item) {
    const minLength = 2;
    const maxLength = 4;

    if (this.callsCount === undefined) {
        const narrowBoundary = 127;

        // eslint-disable-next-line fp/no-mutation
        this.callsCount = (item > narrowBoundary
            ? maxLength
            : minLength);
    }

    // eslint-disable-next-line fp/no-mutation
    this.callsCount -= 1;

    if (this.callsCount <= 0) {
        // eslint-disable-next-line fp/no-delete
        delete this.callsCount;

        return true;
    }

    return false;
}

function formatAsExtendableNumber(arrayOfBytes) {
    // eslint-disable-next-line fp/no-mutation
    arrayOfBytes[0] &= 0x7F;

    const maxByteValue = 256;

    return arrayOfBytes.reduce((res, item) => (res * maxByteValue) + item);
}

module.exports = {
    parseAsExtendableByte,
    parseAsExtendableShort,
    formatAsExtendableNumber
};
