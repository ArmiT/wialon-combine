/*jslint node, bitwise */
/*eslint no-magic-numbers: "off", no-bitwise: "off" */

"use strict";

const R = require("ramda");
const {responseStartCondition, response} = require("./wialon");

const {
    success,
    authError,
    wrongPwd,
    error,
    crcError
} = response;

function makeResponse(code, sequence) {
    return R.concat(
        responseStartCondition,
        [
            code,
            sequence >> 8,
            sequence & 0xFF
        ]
    );
}

function makeSuccessfulResponse(sequence) {
    return makeResponse(success, sequence);
}

function makeAuthErrorResponse(sequence) {
    return makeResponse(authError, sequence);
}

function makePwdErrorResponse(sequence) {
    return makeResponse(wrongPwd, sequence);
}

function makeFailureResponse(sequence) {
    return makeResponse(error, sequence);
}

function makeCrcErrorResponse(sequence) {
    return makeResponse(crcError, sequence);
}

module.exports = Object.freeze({
    makeResponse,
    makeSuccessfulResponse,
    makeAuthErrorResponse,
    makePwdErrorResponse,
    makeFailureResponse,
    makeCrcErrorResponse
});
