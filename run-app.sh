#!/bin/sh

set -eu

docker run -it --rm -w /app -v $PWD:/app twinscom/node:git $@
